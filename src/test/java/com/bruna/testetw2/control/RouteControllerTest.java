package com.bruna.testetw2.control;

import com.bruna.testetw2.model.Route;
import com.bruna.testetw2.service.RouteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Bruna Tofani
 */

public class RouteControllerTest {
    
    @Autowired
    private RouteService routeService;
    
    public RouteControllerTest() {
        
        Route route1 = new Route("A","B",5L, "AB5");
        Route route2 = new Route("B","C",4L, "BC4");
        Route route3 = new Route("C","D",8L, "CD8");
        Route route4 = new Route("D","C",8L, "DC8");
        Route route5 = new Route("D","E",6L, "DE6");
        Route route6 = new Route("A","D",5L, "AD5");
        Route route7 = new Route("C","E",2L, "CE2");
        Route route8 = new Route("E","B",3L, "EB3");
        Route route9 = new Route("A","E",7L, "AE7");
        
        routeService.addRoute(route1);
        routeService.addRoute(route2);
        routeService.addRoute(route3);
        routeService.addRoute(route4);
        routeService.addRoute(route5);
        routeService.addRoute(route6);
        routeService.addRoute(route7);
        routeService.addRoute(route8);
        routeService.addRoute(route9);
    }
    
    /**
     * Test of prepareRoute method, of class RouteController.
     */
    @org.junit.Test
    public void testGetBetterRoute_PassingNull() {
        System.out.println("getBetterRoute_PassingNull");
        String way = null;
        RouteController instance = new RouteController();
        String expResult = "NO SUCH ROUTE";
        String result = instance.getBetterRoute(way);
        assertEquals(expResult, result);
        
        fail("testGetBetterRoute_PassingNull fail");
    }
    
    /**
     * Test of prepareRoute method, of class RouteController.
     */
    @org.junit.Test
    public void testGetBetterRoute_ReturnNoSuchRoute() {
        System.out.println("getBetterRoute_ReturnNoSuchRoute");
        String way = "CEBCDC";
        RouteController instance = new RouteController();
        String expResult = "NO SUCH ROUTE";
        String result = instance.getBetterRoute(way);
        assertEquals(expResult, result);
        
        fail("testGetBetterRoute_ReturnNoSuchRoute fail");
    }
    
    /**
     * Test of prepareRoute method, of class RouteController.
     */
    @org.junit.Test
    public void testGetBetterRoute() {
        System.out.println("getBetterRoute");
        String way = "CDC";
        RouteController instance = new RouteController();
        String expResult = "9";
        String result = instance.getBetterRoute(way);
        assertEquals(expResult, result);
        
        fail("getBetterRoute fail");
    }
    
}
