package com.bruna.testetw2.control;

import com.bruna.testetw2.model.Route;
import com.bruna.testetw2.service.RouteService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Bruna Tofani
 */
@Controller("routeController")
public class RouteController {
 
    private RouteService routeService;
    private List<Route> allRoutes;
    
    RouteController(){
        allRoutes = routeService.getAllRoutes();
    }
    
//    Get the min way from the code 'ABD' routes
    public String getBetterRoute(String way){
        if(way == null)
            return "NO SUCH ROUTE";
        
        String stations [] = getStations(way);
        Long distance = 0L;
        
        for(int i=0; i< stations.length; i++){
            if(getBetterOneWay(stations[i], stations[i + 1]) == null) 
                return "NO SUCH ROUTE";
            distance += getBetterOneWay(stations[i], stations[i + 1]);
        }
        
        return distance.toString();
    }
    
    //    Get the best way from origin to destiny
    private Long getBetterOneWay(String origin, String destiny){
       
        Long distance = 0L;
        Long minDistance = 0L;
        List<Route> sameOrigin = this.getSameOrigin(origin);
        List<List<Route>> possibleRoutes = new ArrayList<List<Route>>();
        
        for(int i=0; i < sameOrigin.size(); i++){
            if(verifyDestiny(sameOrigin.get(i), destiny))
                possibleRoutes.add(getPossibleRoute(sameOrigin.get(i), destiny));
            return null;
        }
        
        return this.verifyBetterRoute(possibleRoutes);
    }         
    
    //  Get the best route from the all routes
    private Long verifyBetterRoute(List<List<Route>> routes){
        List<Long> distances = new ArrayList<Long>();
        for(int i=0; i < routes.size(); i++){
            List<Route> way = routes.get(i);
            Long distance = 0L;
            for(int k=0; k < way.size(); k++){
                distance += way.get(k).getDistance();
            }
            distances.add(distance);
        }
        return getMinDistance(distances);
    }
    
    //   get the min distance from all points distances 
    private Long getMinDistance(List<Long> distances){
        Long min = distances.get(0);
        for(int k=1; k <= distances.size(); k++){
            min = (min > distances.get(k) ? distances.get(k) : min );
        }
        return min;
    }
    
    // Verify if the route ends on the destiny choose
    private boolean verifyDestiny(Route route, String destiny){
        for(int j=0; j < this.allRoutes.size(); j++){
            Route next = this.allRoutes.get(j);
            if(next.getOrigin().equalsIgnoreCase(route.getDestiny())){
                if(!next.getDestiny().equalsIgnoreCase(destiny))
                    verifyDestiny(next, destiny);
                else
                    return true;
            }
        }
        return false;
    }
    
    // Get the possible route from the one point to another
    private List<Route> getPossibleRoute(Route route, String destiny){
        
        List<Route> possibleRoute = new ArrayList<Route>();
        
        for(int j=0; j < this.allRoutes.size(); j++){
            Route next = this.allRoutes.get(j);
            if(next.getOrigin().equalsIgnoreCase(route.getDestiny())){
                if(!next.getDestiny().equalsIgnoreCase(destiny))
                    getPossibleRoute(next, destiny);
                else
                    possibleRoute.add(route);
            }
        }
        return possibleRoute;
    }
    
    // Get all routes from the Origin 'A'
    private List<Route> getSameOrigin(String origin){
        
        List<Route> sameOrigin = new ArrayList<Route>();
        for(int j=0; j < this.allRoutes.size(); j++){
            Route route = this.allRoutes.get(0);
            if (route.getOrigin().equalsIgnoreCase(origin))
                sameOrigin.add(route);
        }
        return sameOrigin;
    }
    
    // Split the stations from the code passed 'ABD'
    public String[] getStations(String code){
        return code.split("");
    }
}
