
package com.bruna.testetw2.service.impl;

import com.bruna.testetw2.dao.RouteDAO;
import com.bruna.testetw2.model.Route;
import com.bruna.testetw2.service.RouteService;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author Bruna Tofani
 */
@Service("routeService")
public class RouteServiceImpl implements RouteService{
    
    private RouteDAO routeDAO;
    
    public void addRoute(Route route){
        if(!routeDAO.containsRoute(route))
            routeDAO.addRoute(route);
    }
    
    public List<Route> getAllRoutes(){
        return routeDAO.getAllRoutes();
    }
    
    public boolean containsRouteByCode(String code){
        return routeDAO.containsRouteByCode(code);
    }
    
    public Route getRouteByCode(String code){
        
        if(!routeDAO.containsRouteByCode(code))
            return null;
        
        return routeDAO.getRouteByCode(code);
    }
}
