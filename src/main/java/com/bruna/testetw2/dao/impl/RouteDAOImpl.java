
package com.bruna.testetw2.dao.impl;

import com.bruna.testetw2.dao.RouteDAO;
import com.bruna.testetw2.model.Route;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Bruna Tofani
 */
@Repository("routeDAO")
public class RouteDAOImpl  implements RouteDAO{
    
    private List<Route> routeList;
    
    public void addRoute(Route route){
        routeList.add(route);
    }
    
    public List<Route> getAllRoutes(){
        return this.getRouteList();
    }
    
    public Route getRouteByCode(String code){
        
        for(int i=0; i < getRouteList().size(); i++){
            Route route = getRouteList().get(i);
            if(route.getCode().equalsIgnoreCase(code))
                return route;
        }
        return null;
    }
    
    public boolean containsRoute(Route route){
        return getRouteList().contains(route);
    }
    
    public boolean containsRouteByCode(String code){
        
        for(int i=0; i < getRouteList().size(); i++){
            Route route = getRouteList().get(i);
            if(route.getCode().equalsIgnoreCase(code))
                return true;
        }
            
        return false;
    }
    
    public void RouteDAO(){
        setRouteList(new ArrayList<Route>());
    }

    /**
     * @return the routeList
     */
    public List<Route> getRouteList() {
        return routeList;
    }

    /**
     * @param routeList the routeList to set
     */
    public void setRouteList(List<Route> routeList) {
        this.routeList = routeList;
    }
}
