package com.bruna.testetw2.service;

import com.bruna.testetw2.model.Route;
import java.util.List;

/**
 *
 * @author Bruna Tofani
 */
public interface RouteService {
 
    public Route getRouteByCode(String code);
    
    public void addRoute(Route route);
    
    public List<Route> getAllRoutes();
}
