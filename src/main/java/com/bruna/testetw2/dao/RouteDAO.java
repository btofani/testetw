package com.bruna.testetw2.dao;

import com.bruna.testetw2.model.Route;
import java.util.List;

/**
 *
 * @author Bruna Tofani
 */
public interface RouteDAO {
    
    public void addRoute(Route route);
    
    public boolean containsRouteByCode(String code);
    
    public Route getRouteByCode(String code);
    
    public boolean containsRoute(Route route);
    
    public List<Route> getAllRoutes();
}
